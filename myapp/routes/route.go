package routes

import (
	"myapp/controller"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)


func InitializedRouter() {
	// resgitering route and mapping the handler function
	router := mux.NewRouter()
	router.HandleFunc("/home", controller.HomeHandler)
	router.HandleFunc("/urlParameter/{myname}", controller.ParameterHandler)
	//student
	router.HandleFunc("/student", controller.AddStudent).Methods("POST")
	router.HandleFunc("/student/{sid}", controller.GetStud).Methods("GET")
	router.HandleFunc("/student/{sid}", controller.UpdateStud).Methods("PUT")
	router.HandleFunc("/student/{sid}", controller.DeleteStud).Methods("DELETE")
	router.HandleFunc("/students", controller.GetAllStuds)

	//admin
	router.HandleFunc("/signup", controller.Signup).Methods("POST")
	router.HandleFunc("/login", controller.Login).Methods("POST")

	//logout
	router.HandleFunc("/logout", controller.Logout)

	// Course
	router.HandleFunc("/course", controller.AddCourse).Methods("POST")
	router.HandleFunc("/course/{cid}", controller.GetCourse).Methods("GET")
	router.HandleFunc("/course/{cid}", controller.UpdateCourse).Methods("PUT")
	router.HandleFunc("/course/{cid}", controller.DeleteCourse).Methods("DELETE")
	router.HandleFunc("/courses", controller.GetAllCourse)

	//Enroll
	router.HandleFunc("/enroll", controller.Enroll).Methods("POST")
	router.HandleFunc("/enroll/{sid}/{cid}", controller.DeleteEnroll).Methods("DELETE")
	router.HandleFunc("/enroll/{sid}/{cid}", controller.GetEnroll)
	router.HandleFunc("/enrolls", controller.GetEnrolls)

	// to serve static files
	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)
		// starting server
	err := http.ListenAndServe(":8085", router)
	if err != nil {
		os.Exit(1)
	}
	
}