package model

import "myapp/datastore/postgres"

type Course struct{
	Cid string `json:"cid"`
	Coursename string `json:"coursename"`
}

const (coursequeryInsert = "INSERT INTO course(cid, coursename) VALUES($1, $2) RETURNING cid;"
queryGetCourse = "SELECT cid, coursename FROM course WHERE cid=$1;"
queryUpdateCourse = "UPDATE course SET cid=$1, coursename=$2 WHERE cid=$3 RETURNING cid;"
queryDeleteCourse = "DELETE FROM course WHERE cid=$1 RETURNING cid;")


func (c *Course) CourseCreate() error{
	// fmt.Println(s) 
	row := postgres.Db.QueryRow(coursequeryInsert, c.Cid, c.Coursename)
	err := row.Scan(&c.Cid)
	return err
}

func (c *Course) CourseRead() error{
	row:= postgres.Db.QueryRow(queryGetCourse, c.Cid) //give the vlaue of Stdid on your own and return the
	err := row.Scan(&c.Cid, &c.Coursename)
	return err
}

func (c *Course) CourseUpdate(oldCID string) error{
	err := postgres.Db.QueryRow(queryUpdateCourse, c.Cid, c.Coursename, oldCID).Scan(&c.Cid)
	return err
}

func (c *Course) CourseDelete() error{
	if err := postgres.Db.QueryRow(queryDeleteCourse, c.Cid).Scan(&c.Cid); err != nil{
		return err
	}
	return nil
}

func GetAllCo() ([]Course, error){
	rows, getErr := postgres.Db.Query("SELECT * FROM course;")
	if getErr != nil{
		return nil, getErr
	}

	courses := []Course{} 

	for rows.Next() {
		var c Course
		dbErr := rows.Scan(&c.Cid, &c.Coursename)
		if dbErr != nil{
			return nil, dbErr
		}

		courses = append(courses, c)
	}
	rows.Close()
	return courses, nil
}