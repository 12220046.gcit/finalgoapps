package controller

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// handler function
func HomeHandler(w http.ResponseWriter, r *http.Request){
	_, err := w.Write([]byte("Hello world"))
	if err != nil {
		fmt.Println("error:", err)
	}
}
func ParameterHandler(w http.ResponseWriter, r *http.Request){
	para := mux.Vars(r)
	name := para["myname"]
	fmt.Println(name)
	_,err := w.Write([]byte("my name is" + name))
	if err != nil {
		fmt.Println("error:", err)
	}
}