package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAdminLogin(t *testing.T){
	url := "http://localhost:8080/login"
	var data  = []byte(`{
  "email": "lolikirasama@gmail.com",
  "password": "Kamyln"
}`)
	// create request object
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Set("Content-type", "application/json")
	// create a client
	client := &http.Client{}
	// send POST request using Do function
	resp, err := client.Do(req)
	if err != nil{
		panic(err)
	}
	defer resp.Body.Close()
	body,_ := io.ReadAll(resp.Body)
	// assert statement
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	// if two json are equal or not
	assert.JSONEq(t, `{"message": "success"}`, string(body))
}


// go get github.com/stretchr/testify/assert