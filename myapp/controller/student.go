package controller

import (
	"database/sql"
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddStudent(w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w,r){
		return 
	}
	var stud model.Student
	// fmt.Println(stud)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&stud)
	if err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid Json Data")
		return
	} 
	
	dbErr := stud.Create()
	// fmt.Println(dbErr)
	if dbErr != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, dbErr.Error())
		return
	}
	// w.Write([]byte ("student data added"))
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message": "student data added"})

}

func getUserId(sid string)(int64){
	ssid,_ := strconv.ParseInt(sid, 10, 64)
	return ssid

}
func GetStud(w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w,r){
		return 
	}
	//get url parameter
	sid := mux.Vars(r)["sid"]
	ssid := getUserId(sid)
	stud := model.Student{StdId: ssid}
	// fmt.Println(stud)
	getErr := stud.Read()
	if getErr != nil{
		switch getErr{
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "student not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
	
	} else{
		httpResp.RespondWithJson(w, http.StatusOK, stud)
	}

}

func UpdateStud(w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w,r){
		return 
	}
	old_sid := mux.Vars(r)["sid"]
	old_stdId:= getUserId(old_sid)
	//student instance
	var stud model.Student
	//json object
	decoder := json.NewDecoder(r.Body)
	
	if err:= decoder.Decode(&stud); err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	getErr := stud.Update(old_stdId)
	if getErr != nil{
		switch getErr{
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "student not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
	
	} else{
		httpResp.RespondWithJson(w, http.StatusOK, stud)
	}
}

func DeleteStud(w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w,r){
		return 
	}
	sid := mux.Vars(r)["sid"]
	stdId:= getUserId(sid)
	s := model.Student{StdId: stdId}
	//student instance
	if err:= s.Delete(); err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"status":"deleted"})
}

func GetAllStuds(w http.ResponseWriter, r *http.Request){
	if !VerifyCookie(w, r){
		return 
	}
	students, getErr := model.GetAllStudents()
	if getErr != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, students)
}