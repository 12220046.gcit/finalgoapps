// package controller

// import (
// 	"encoding/json"
// 	"myapp/model"
// 	"myapp/utils/httpResp"
// 	"net/http"
// )

// func Signup(w http.ResponseWriter, r *http.Request){
// 	var admin model.Admin
// 	// fmt.Println(stud)
// 	decoder := json.NewDecoder(r.Body)
// 	err := decoder.Decode(&admin)
// 	if err != nil{
// 		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid Json Data")
// 		return
// 	}

// 	saveErr := admin.Create()
// 	// fmt.Println(saveErr)
// 	if saveErr != nil{
// 		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
// 		return
// 	}
// 	// w.Write([]byte ("student data added"))
// 	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message": "admin added"})
// }

package controller

import (
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"
	"time"
)

var admin model.Admin

func Signup( w http.ResponseWriter, r *http.Request){
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&admin);err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	saveErr:= admin.Create()
	if saveErr != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	// //no error
	httpResp.RespondWithJson(w, http.StatusCreated, map[string]string{"status": "admin added"})
}
func Login(w http.ResponseWriter, r *http.Request) {
	// var admin model.Admin
	err := json.NewDecoder(r.Body).Decode(&admin)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	
	getErr := admin.Get()
	if getErr != nil {
	httpResp.RespondWithError(w, http.StatusUnauthorized, getErr.Error())
	return
	}
	// creatingcookie 
	cookie := http.Cookie{
		Name: "my-cookie",
		Value: "cookie-value", //info of the user is stored in value
		Expires: time.Now().Add(20 * time.Minute),
		Secure: true,
	}
	//set cookie and sent back to client
	http.SetCookie(w, &cookie)
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message":"success"})
}

func Logout (w http.ResponseWriter, r *http.Request){
	http.SetCookie(w, &http.Cookie{
		Name: "my-cookie",
		Expires: time.Now(),
	})
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message": "logout success"})
}

func VerifyCookie(w http.ResponseWriter, r *http.Request) bool{
	cookie, err := r.Cookie("my-cookie")
	if err != nil{
		if err == http.ErrNoCookie{
			httpResp.RespondWithError(w, http.StatusSeeOther, "cookie not set")
			return false
		}
		httpResp.RespondWithJson(w, http.StatusInternalServerError, "internal server eror")
		return false
	}

	//validate cookie value
	if cookie.Value != "cookie-value"{
		httpResp.RespondWithError(w, http.StatusSeeOther, "Cookie value does not match")
		return false
	}
	return true
}