package controller

import (
	"database/sql"
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"

	"github.com/gorilla/mux"
)

func AddCourse(w http.ResponseWriter, r *http.Request){
	var course model.Course
	// fmt.Println(course)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&course)
	if err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid Json Data")
		return
	}

	dbErr := course.CourseCreate()
	// fmt.Println(dbErr)
	if dbErr != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, dbErr.Error())
		return
	}
	// w.Write([]byte ("student data added"))
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message": "course data added"})

}

func GetCourse(w http.ResponseWriter, r *http.Request){
	//get url parameter
	cid := mux.Vars(r)["cid"]
	coursey := model.Course{Cid: cid}
	// fmt.Println(coursey)
	getErr := coursey.CourseRead()
	if getErr != nil{
		switch getErr{
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Course not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
	} else{
		httpResp.RespondWithJson(w, http.StatusOK, coursey)
	}

}

func UpdateCourse(w http.ResponseWriter, r *http.Request){
	old_cid := mux.Vars(r)["cid"]
	//student instance
	var course model.Course
	//json object
	decoder := json.NewDecoder(r.Body)
	if err:= decoder.Decode(&course); err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	getErr := course.CourseUpdate(old_cid)
	if getErr != nil{
		switch getErr{
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Course not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}

	} else{
		httpResp.RespondWithJson(w, http.StatusOK, course)
	}
}

func DeleteCourse(w http.ResponseWriter, r *http.Request){
	cid := mux.Vars(r)["cid"]
	c := model.Course{Cid: cid}
	//student instance
	if err:= c.CourseDelete(); err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"status":"deleted"})
}
func GetAllCourse(w http.ResponseWriter, r *http.Request){
	courses, getErr := model.GetAllCo()
	if getErr != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, courses)
}